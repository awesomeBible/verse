---
name: "Bilder hinzufügen"
about: "Du willst Bilder zu awesomeBible Verse hinzufügen!"
title: "[Bild hinzufügen]"
ref: "main"
labels:

- bild

---

<!-- 
Bitte lies alles so sorgfältig wie möglich durch und fülle das Issue so weit wie dir möglich aus. 

Du kannst Checkboxen anhaken, indem du zwischen die zwei eckigen Klammern ein "x" setzt. (- [ ] wird zu - [x])
-->


## Bildrechte
- [ ] Ich habe das Bild selbst erstellt. Mir gehören alle Bildrechte.
- [ ] Das Bild oder Teile davon sind nicht von mir, aber unter einer der [hier](https://codeberg.org/awesomeBible/verse/wiki/Mitmachen) gelisteten Lizenzen freigegeben.

- Bildquelle (Credits): 