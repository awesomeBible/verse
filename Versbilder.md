# Versbilder herunterladen
Du kannst die Versbilder für awesomeBible Verse hier herunterladen. Die Bilder sind nach Jahren sortiert.

Die Bilder sind auf Servern gehostet, die awesomeBible gehören. Sie werden aber auch um die Serverlast zu reduzieren auf andere Filehoster gespiegelt.

Unten kannst du einen Mirror auswählen.
## 2023
Die SHA256-Hashsumme der Datei "2022.zip" ist `9A2202F4444FEFC590435EF259C105E685DEE2494E50F3FCFDA676634F73BB9D`.

- [awesomeBible](https://verse.awesomebible.de/releases/2023.zip)
- [GitHub](https://github.com/awesomebible/verse/releases/download/img-2023/2023.zip)
- [OSDN](https://osdn.net/projects/awesomebible-verse/downloads/78253/2023.zip/)

## 2022
Die SHA256-Hashsumme der Datei "2022.zip" ist `BC3D8BAC389A60AFDC5D0AB8258D1A6771D4F699D4BA4CD1A0CBF3ADC1A14F61`.

- [awesomeBible](https://verse.awesomebible.de/releases/2022.zip)
- [GitHub](https://github.com/awesomebible/verse/releases/download/img-2022/2022.zip)
- [OSDN](https://osdn.net/projects/awesomebible-verse/downloads/76604/2022.zip/)

## 2021
Die SHA256-Hashsumme der Datei "2021.zip" ist `73BFB195B37A215D91147093191B7B5002B99478D598D2D45F90CBB2A095F1D3`.

- [awesomeBible](https://verse.awesomebible.de/releases/2021.zip)
- [GitHub](https://github.com/awesomebible/verse/releases/download/img-2021/2021.zip)
- [OSDN](https://osdn.net/projects/awesomebible-verse/downloads/76603/2021.zip/)