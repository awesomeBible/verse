# awesomeBible Verse 🖼️
## Versbilder leicht gemacht.
[![](https://img.shields.io/badge/Lizenz-AGPLv3-orange?cacheSeconds=31536000)](https://codeberg.org/awesomeBible/verse/src/branch/main/LICENSE) [![](https://img.shields.io/matrix/awesomebible_verse:matrix.org?color=orange&logo=matrix&cacheSeconds=3600)](https://matrix.to/#/#awesomebible_verse:matrix.org)

awesomeBible Verse hat zum Ziel Versbilder auf eine einfache Weise zur Verfügung zu stellen.

Um awesomeBible Verse zu nutzen und den Vers des Tages einzubinden, musst du nur ``https://verse.awesomebible.de`` als Bildquelle angeben.

Um ein spezifisches Bild zu finden, kann der [Versepicker](https://versepicker.netlify.app/) benutzt werden: [Zum Verspicker](https://versepicker.awesomebible.de/).

![Animation vom einbinden von awesomeBible Verse in einem WordPress Post](https://codeberg.org/awesomeBible/verse/raw/branch/readme-images/preview.gif)

### [Installation](https://codeberg.org/awesomeBible/verse/wiki/Installation) :wrench:
### [Öffentliche API](https://codeberg.org/awesomeBible/verse/wiki/%C3%96ffentliche-API) :globe_with_meridians:
### [Mitmachen](https://codeberg.org/awesomeBible/verse/wiki/Mitmachen) :smiley:
### [Mitwirkende](https://codeberg.org/awesomeBible/verse/wiki/Mitwirkende) :sparkling_heart:
### [Community](https://matrix.to/#/#awesomeBible:matrix.org) :unicorn:

* * *

### Vorschau :star2:
[![awesomeBible Verse Bild](https://wsrv.nl/?url=verse.awesomebible.de&maxage=1d&w=640&output=webp)](https://verse.awesomebible.de/)